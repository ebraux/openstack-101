---
hide:
  - navigation
  - toc
---

# openstack - 101

Concepts de base et prise en main

---

Slides de la présentation:

- Version en ligne: [openstack-101.html](openstack-101.html)
- version PDF: [openstack-101.pdf](openstack-101.pdf)

---

Les labs correspondants à cette session sont disponibles :

- version en ligne : [https://ebraux.gitlab.io/openstack-labs/](https://ebraux.gitlab.io/openstack-labs/)
- dépôt Gitlab : [https://gitlab.com/ebraux/openstack-labs](https://gitlab.com/ebraux/openstack-labs)

---

![image alt <>](assets/openstack.png)

