---
marp: true
paginate: true
theme: default
---
<!-- #3eaff3 bleu ciel -->
<!-- #0074d0 bleu electrique -->
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:40% 90%](assets/openstack.png)

# Openstack 101

## Concepts et principes de base

-emmanuel.braux@imt-atlantique.fr-

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)

# Openstack 101

- **Définition et concepts**
- **L’écosystème OpenStack**
- **Utiliser OpenStack**
- **Les Principes d’utilisation**
- **Les Composants d'Openstack**
- **Détail des Principaux Composants**

---

# Licence informations


Auteur : -emmanuel.braux@imt-atlantique.fr-

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)

# Openstack 101

- ## > Définition et concepts
- *L’écosystème OpenStack*
- *Utiliser OpenStack*
- *Les Principes d’utilisation*
- *Les composants d'Openstack*
- *Détail des principaux composants*

---

# Définitions

## Wikipédia
OpenStack est un ensemble de logiciels open source permettant de déployer des infrastructures de cloud Computing (IaaS).

## Openstack-org

OpenStack is a cloud operating system that controls large pools of compute, storage, and networking resources throughout a datacenter, all managed through a dashboard that gives  administrators control while empowering their users to provision resources through a web interface. 

---
# Framework Iaas Modulaire

- **IaaS**
  - Infrastructure libre service, à la demande
- **Framework**
  - Ensemble de composants logiciels
  -  Bases d'une infrastructure IT
- **Modulaire**
  - un ensemble de modules, pas tous nécessaires
  - chaque module peut être configuré en fonction de besoins
  - chaque module peut-être étendu (drivers)

>*OpenStack - Cloud Computing d'entreprise, Infrastructure as a Service (IaaS) - Sébastien Déon*
---

# Openstack d'un Point de vue Technique

- C'est un "Cluster de serveurs" : 
  - Les utilisateurs échangent avec la partie "control plane"
  - Les ressources sont déployées sur des noeuds de "compute"
- Tout matériel peut être intégré (en théorie)
- Utilisation de matériel standard, en grand nombre.
- Orchestrés par des logiciels de gestion et d'automatisation
- Très complexe à gérer pour les administrateurs de plateforme de cloud

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)


# Openstack 101

- *Définition et concepts*
- ## > L’écosystème OpenStack
- *Utiliser OpenStack*
- *Les Principes d’utilisation*
- *Les composants d'Openstack*
- *Détail des principaux composants*


---
![bg left:30% 60%](img/openstack-logo.png)


# Historique

2010 : **Rackspace Hosting** + **NASA** lancent un projet communautaire et Open source

Objectif : créer et offrir des services de Cloud Computing avec du matériel standard

---
# L' Open Infrastructure Foundation

- Fondation Openstack, créé en 2012, indépendante : gouvernance principale du projet
- Devenue "Open Infrastructure Foundation" fin 2020 pour s'ouvrir à d'autres projets
- participation ouverte: entreprises, sponsors, membres individuels

*[https://openinfra.dev/](https://openinfra.dev/)*
*[https://www.openstack.org/community/supporting-organizations/](https://www.openstack.org/community/supporting-organizations/)*

---
# L'Open Infrastructure Summit

- Anciennement "Openstack summit"
- Aux USA jusqu’en 2013
- Aujourd’hui : alternance USA et Asie/Europe
- Quelques centaines au début à 4500 de participants aujourd’hui
- En parallèle : conférence (utilisateurs, décideurs) et Design Summit (développeurs)
- Détermine le nom de la release : lieu/ville à proximité du Summit

*[https://www.openstack.org/summit/](https://www.openstack.org/summit/)*

---

# Quelques soutiens/contributeurs

- Rackspace et la NASA
- Canonical, Red Hat/IBM, HPE, Dell, Intel
- Huawei, Cisco, Juniper,
- NetApp, VMWare
- Yahoo, Bull
- Mirantis, StackOps,...
- ...
  

*[https://openinfra.dev/companies/](https://openinfra.dev/companies/)*

---
# ... et utilisateurs

- Tous les contributeurs précédemment cités
- Flexible Engine (Orange) et OVH Cloud
- CERN,
- Wikimedia
- Paypal, Comcast, Etc.

Sans compter les implémentations confidentielles

*[https://www.openstack.org/use-cases/](https://www.openstack.org/use-cases/)*

---
# Packaging et développement d'OpenStack

- Openstack est développé en python,
- Le Cycle de vie des versions est assez soutenu : 2 releases annuelles, 1 version "stable" + 2 versions "supportées" *[https://releases.openstack.org/](https://releases.openstack.org/)* 
- Le code source est disponible : *[https://opendev.org/openstack](https://opendev.org/openstack)*
- Openstack est intégré à la plupart des distributions Linux :  RPM, DEB, ...
- Des éditeurs proposent leur propre distribution d'Openstack : Mirantis, HPE, Nokia, Ericsson, … *[https://www.openstack.org/marketplace/distros/](https://www.openstack.org/marketplace/distros/)*
- Il existe des projets de déploiement avec des outils d'automatisation (ansible, puppet, ..) *[https://docs.openstack.org/openstack-ansible/latest/](https://docs.openstack.org/openstack-ansible/latest/)*

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)


# Openstack 101

- *Définition et concepts*
- *L’écosystème OpenStack*
- ## > Utiliser OpenStack
- *Les Principes d’utilisation*
- *Les composants d'Openstack*
- *Détail des principaux composants*



---
# Pourquoi utiliser Openstack ?

- Mis à part pour les 3 leaders (AWS, Azure et GCP), Openstack est utilisé par la plupart des fournisseur de cloud public : OVH Cloud, Orange cloud, Oracle IBM, ...

- Openstack est disponible dans la plupart des établissements d'enseignement recherche : CERN, IN2P3, Frances grilles, IMT Atlantique, UBO,  Ifremer...

- Openstack est également présent dans les Entreprises.

# Ce que vous ferez avec Openstack sera réutilisable et portable !!



---
# Approche "Simple"

## Le dashboard Openstack

![bg right:50% 90%](img/horizon-accueil.png)

---
# Approche "Efficace"

## Le client en ligne de commande 

```bash
> openstack
``` 

- Développé en python,
- L’authentification se fait en passant les "credentials" par paramètres ou variables d’environnement

> *[https://docs.openstack.org/python-openstackclient/latest/](https://docs.openstack.org/python-openstackclient/latest/)*

---
# Approche "Système"

## Les outils/toolkits tierces 

- outils de déploiement/automatisation : Ansible, terraform, ..
- Toolkits génériques : jcloud, ...

> *[https://docs.ansible.com/ansible/latest/collections/openstack/cloud](https://docs.ansible.com/ansible/latest/collections/openstack/cloud)*
> *[https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs)*
> *[https://jclouds.apache.org/](https://jclouds.apache.org/)*

---
# Approche "Développement"

## Intégration à vos développement 
Les Software Development Kits (SDK)

- SDK principal en python
- Disponibles pour la plupart des langages : JAVA, GO, PHP, Perl, C, ...

>*[https://docs.openstack.org/openstacksdk/latest/](https://docs.openstack.org/openstacksdk/latest/)*
>*[https://wiki.openstack.org/wiki/SDKs](https://wiki.openstack.org/wiki/SDKs)*

---
# Pour tout le reste

## L'API d'Openstack
- Les clients (y compris Horizon) utilisent l’API
- Des crédentials sont nécessaires
  - utilisateur + mot de passe + projet
- Direct, en HTTP, via des outils comme curl
> L’option "--debug" du CLI permet d'afficher les échanges HTTP

---

<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)



# Openstack 101

- *Définition et concepts*
- *L’écosystème OpenStack*
- *Utiliser OpenStack*
- ## > Les Principes d’utilisation
- *Les composants d'Openstack*
- *Détail des principaux composants*



---

# Les Grands principes du Iaas s'appliquent

- Les instances sont éphémères, les données sont persistantes
- On est prêt à perdre l'instance, mais pas le service.
- C’est l’application qui doit assurer la continuité du service, pas (plus) l’infrastructure.
- Toutes les opérations peuvent être réalisées par des appels via les API REST.

---
# Compute
- "image", "gabarit", "instance", "métadonnées", "cloudinit"
- On lance une instance à partir d’une image, en lui affectant des ressources via un gabarit.
- Une image se configure/personnalise lors de son instanciation grâce à l’API de metadata, et le mécanisme Cloud-Init
- On n’utilise plus de mot de passe pour se connecter aux instances, mais des paires de clés.

---
# Volumes
- Stockage de type "bloc" ou "objet"
- Les données doivent être stockée sur un stockage persistant, un volume

# Réseau
- "routeur", "réseau", "sous réseau", "groupes de sécurité", "IP Flottantes"
- Pour pouvoir accéder à une instance, il faut lui affecter une IP flottante, et définir des règles de sécurité.  

---
# Spécificités Openstack

## Gestion des utilisateurs

- Projet (Project/Tenant) : unité d’appartenance de base = propriétaire des ressources.
- Utilisateur (User) : compte autorisé à utiliser les API
- Quota : contrôle l’utilisation des ressources (vcpu, ram, fip, security groups,...) dans un projet.

**On associe les ressources à un projet, via des quotas. Les utilisateurs deviennent membre des projets.**

---
# Spécificités Openstack

## Routeurs réseau

- Dans la plupart des solution de cloud, l'interconnexion entre les réseaux se configure avec des tables de routage.
- Openstack propose des "routeurs.


---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)


# Openstack 101

- *Définition et concepts*
- *L’écosystème OpenStack*
- *Utiliser OpenStack*
- *Les Principes d’utilisation*
- ## > Les composants d'Openstack
- *Détail des principaux composants*



---
# L'architecture 

Un ensemble de composants indépendants et autonomes

Une architecture hautement "scalable"

Toutes les fonctionnalités doivent être accessibles par l’API


---
# Composants principaux

![height:500px](img/openstack-main-components.png)

---
# Modèle "Big Tent" 

![height:600px](img/big-tent-and-core-services.png)

---

# Les Composants« Core »

- Authentification, Autorisations (Identity Service) : **Keystone**
- Réseau (Networking) : **Neutron**
- Gestion/execution des instances (Compute) : **Nova**
- Catalogue de services : **Keystone**
- Registre d’images (Image Service) : **Glance**
- Stockage :
  - block (Block Storage) : **Cinder**
  - Stockage objet (Object Storage ) : **Swift**  

---
# Les autres Composants  « Big Tent »

- Console web (Dashboard) : **Horizon**
- Orchestration des ressources (Orchestration) : **Heat**
- Collecte de métriques (Metering) : **Ceilometer**
- Base de données : **Trove**
- DNS : **Designate**
- Load Balancer : **Octavia** 
- ...

*[https://www.openstack.org/software/](https://www.openstack.org/software/)*

---

# Map of Openstack Projects

![height:550px](img/openstack-map-v20240401.png)

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)


# Openstack 101

- *Définition et concepts*
- *L’écosystème OpenStack*
- *Utiliser OpenStack*
- *Les Principes d’utilisation*
- *Les composants d'Openstack*
- ## > Détail des principaux composants
---
# Identity Service : Keystone

![bg left:30% 80%](img/keystone-logo.png)

- Service de renseignements (utilisateurs, groupes, projets. . . )
- Authentifie les utilisateurs en leur donnant des jetons
- Valide l’authenticité d’un jeton auprès des services
- Dépositaire du catalogue de service

---
# Keystone Architecture

- Un web-service : répond à des requêtes HTTP en XML ou JSON (default).
- Écrit en Python, Accès via Apache
- Backend de gestion des identités : SQL, LDAP, AD, ...

![h:300 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/keystone-architecture.png) 

---
# Image Service : Glance

![bg left:30% 80%](img/glance-logo.png)
- Registre d’images
- Stocke et distribue les images d’instance
- Appelé par Compute (Nova) lors de l’instanciation d’une nouvelle Instance.
- Format des images : raw, vmdk, qcow , iso, ...
- Gère les métadonnées des images (type, OS, architecture. . . )

---
# Glance - Architecture

![bg left:30% 70% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/glance-architecture.png)

- Une API
- Une base de données
- Plusieurs backend de stockage des images :
  - Système de fichier local ou NFS (NAS/SAN)
  - Swift (OpenStack Object Service)
  - S3 (Object Service Amazon)
  - Ceph
  - ...

---
# Compute Service : Nova
![bg left:30% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/nova-logo.png)

- Gère le cycle de vie des instances
- S'interface avec les hyperviseurs : KVM, Xen, ESX, Hyper-V, ...
- Fournit les métadonnées (systèmes et utilisateurs) aux instances
-  Fournit un accès aux instances (console, VNC, Spice. . . )

---
# Nova - Architecture


![bg left:45% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/nova-architecture.png)


- Une API : nova-api
- Les services de gestion sur les "controler nodes" : scheduler, conductor, api-metadata
- Le service d'interfaçage avec l'hyperviseur sur les "compute nodes" : nova-compute


---
# Network Service : Neutron

![bg left:30% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/neutron-logo.png)

- Virtualisation du réseau
  - routeurs, switch
  - DNS, DHCP, Firewall, ...
- S'interface avec les provider réseau : LinuxBrige, Open vSwitch, Open Virtual Network, Cisco virtual and physical switches,VMware NSX, ...
- Fourni les interfaces réseau aux instances déployées par Nova

---
# Neutron - Architecture 

![bg left:40% 85% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/neutron-architecture.png)

- Expose une API, via le Neutron serveur
- Configuration modulaire :  plugins et agents
- Forte dépendance au "Network Provider"
- API Unifiée, pour piloter plusieurs architectures réseau.

---
# Block Storage Service : Cinder

![bg left:30% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/cinder-logo.png)

- Gestion des volumes
- Stockage en mode "bloc"
- Gère un ou plusieurs espaces de stockage
- Backend de stockage : LVM, iSCSI, ...

---
# Cinder - Architecture

![bg left:40% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/cinder-architecture.png)


- Expose une API
  - 2 versions cohabitent V2 et V3
- Utilise l’authentification keystone
- Services :
  - Cinder Server Node
  - Cinder Storage Node

---
# Object Storage service : Swift

![bg left:30% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/swift-logo.png)
- Stockage Objet (equivalent AWS S3)
  - Fichiers, vidéos, ...(sous forme "binaire")
  - Des métadonnées associées 
  - Accessibles via une API REST
- Intégre le stockage, la sauvegarde et l'archivage des données
- Principes : 
  - Les **objets** sont stockés dans des **containers**
  - Les **containers** appartiennent à des **accounts**

---
# Swift - Architecture

![bg left:45% 85% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/swift-architecture.png)

## **Proxy Nodes**
- Interface avec les utilisateurs
- API REST
- Gère la disponibilité des storage nodes 
## **Storage Nodes**
- Assure la gestion des données

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)


# Conclusion

---

# En conclusion

- Projet Opensource, viable, géré par une large communauté
- Rythme Évolution rapide
- Ensemble de briques
- Architecture complexe
